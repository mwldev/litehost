package com.litehost.core.connection.transfer;

import org.junit.jupiter.api.*;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ChunkedByteBufferTest {
    private ChunkedByteBuffer chunkedByteBuffer;

    @BeforeAll
    void setUp() {
        this.chunkedByteBuffer = new ChunkedByteBuffer(256, 2);
    }

    @BeforeEach
    void clear() {
        this.chunkedByteBuffer.clear();
    }

    private enum TypeHandler {
        BYTE(Byte.BYTES) {
            @Override
            public Number get(ChunkedByteBuffer buffer) {
                return buffer.get();
            }

            @Override
            public void put(Number number, ChunkedByteBuffer buffer) {
                buffer.put(number.byteValue());
            }
        }, SHORT(Short.BYTES) {
            @Override
            public Number get(ChunkedByteBuffer buffer) {
                return buffer.getShort();
            }

            @Override
            public void put(Number number, ChunkedByteBuffer buffer) {
                buffer.putShort(number.shortValue());
            }
        }, INT(Integer.BYTES) {
            @Override
            public Number get(ChunkedByteBuffer buffer) {
                return buffer.getInt();
            }

            @Override
            public void put(Number number, ChunkedByteBuffer buffer) {
                buffer.putInt(number.intValue());
            }
        }, LONG(Long.BYTES) {
            @Override
            public Number get(ChunkedByteBuffer buffer) {
                return buffer.getLong();
            }

            @Override
            public void put(Number number, ChunkedByteBuffer buffer) {
                buffer.putLong(number.longValue());
            }
        }, FLOAT(Float.BYTES) {
            @Override
            public Number get(ChunkedByteBuffer buffer) {
                return buffer.getFloat();
            }

            @Override
            public void put(Number number, ChunkedByteBuffer buffer) {
                buffer.putFloat(number.floatValue());
            }
        }, DOUBLE(Double.BYTES) {
            @Override
            public Number get(ChunkedByteBuffer buffer) {
                return buffer.getDouble();
            }

            @Override
            public void put(Number number, ChunkedByteBuffer buffer) {
                buffer.putDouble(number.doubleValue());
            }
        };

        private final int size;

        TypeHandler(int size) {
            this.size = size;
        }

        public abstract Number get(ChunkedByteBuffer buffer);

        public abstract void put(Number number, ChunkedByteBuffer buffer);

        public int getSize() {
            return this.size;
        }
    }

    private void put(TypeHandler typeHandler, Number value) {
        int previousPosition = this.chunkedByteBuffer.position();
        typeHandler.put(value, this.chunkedByteBuffer);
        this.assertPositionMoved(previousPosition, typeHandler);
    }

    private void assertPositionMoved(int previousPosition, int toMove) {
        assertEquals(previousPosition + toMove, this.chunkedByteBuffer.position());
    }

    private void assertPositionMoved(int previousPosition, TypeHandler typeHandler) {
        this.assertPositionMoved(previousPosition, typeHandler.getSize());
    }

    private void put(String sequence) {
        int previousPosition = this.chunkedByteBuffer.position();
        this.chunkedByteBuffer.putSequence(sequence, StandardCharsets.UTF_16);
        int expected = sequence.getBytes(StandardCharsets.UTF_16).length;
        this.assertPositionMoved(previousPosition, expected);
    }

    private void testGetSequence() {
        String sequence = "Java is a cool and original language, unlike .Net";
        this.put(sequence);
        this.chunkedByteBuffer.flip();
        assertEquals(sequence, this.chunkedByteBuffer.getSequence(sequence.length(), StandardCharsets.UTF_16));
    }

    private void get(TypeHandler typeHandler, Number toCompare) {
        int previousPosition = this.chunkedByteBuffer.position();
        Number number = typeHandler.get(this.chunkedByteBuffer);
        this.assertPositionMoved(previousPosition, typeHandler);
        assertEquals(number, toCompare);
    }

    private void testGet(TypeHandler typeHandler, Number value) {
        this.put(typeHandler, value);
        this.chunkedByteBuffer.flip();
        this.get(typeHandler, value);
    }

    @Test
    void trim() {
    }

    @Test
    void put() {
        this.put(TypeHandler.BYTE, Byte.MAX_VALUE);
    }

    @Test
    void putShort() {
        this.put(TypeHandler.SHORT, Short.MAX_VALUE);
    }

    @Test
    void putInt() {
        this.put(TypeHandler.INT, Integer.MAX_VALUE);
    }

    @Test
    void putLong() {
        this.put(TypeHandler.LONG, Long.MAX_VALUE);
    }

    @Test
    void putFloat() {
        this.put(TypeHandler.FLOAT, Float.MAX_VALUE);
    }

    @Test
    void putDouble() {
        this.put(TypeHandler.DOUBLE, Double.MAX_VALUE);
    }

    @Test
    void putSequence() {
        this.put();
    }

    @Test
    void get() {
        this.testGet(TypeHandler.BYTE, Byte.MAX_VALUE);
    }

    @Test
    void getShort() {
        this.testGet(TypeHandler.SHORT, Short.MAX_VALUE);
    }

    @Test
    void getInt() {
        this.testGet(TypeHandler.INT, Integer.MAX_VALUE);
    }

    @Test
    void getLong() {
        this.testGet(TypeHandler.LONG, Long.MAX_VALUE);
    }

    @Test
    void getFloat() {
        this.testGet(TypeHandler.FLOAT, Float.MAX_VALUE);
    }

    @Test
    void getDouble() {
        this.testGet(TypeHandler.DOUBLE, Double.MAX_VALUE);
    }

    @Test
    void getSequence() {
        this.testGetSequence();
    }

    @Test
    void typicalUsageTest() {
        String word = "java";
        for (int i = 0; i < 10; i++) {
            this.chunkedByteBuffer.putLong(Long.MAX_VALUE);
            this.chunkedByteBuffer.putInt(Integer.MAX_VALUE);
            this.chunkedByteBuffer.putSequence(word, StandardCharsets.UTF_16);
        }
        this.chunkedByteBuffer.flip();
        for (int i = 0; i < 10; i++) {
            assertEquals(Long.MAX_VALUE, this.chunkedByteBuffer.getLong());
            assertEquals(Integer.MAX_VALUE, this.chunkedByteBuffer.getInt());
            assertEquals(word, this.chunkedByteBuffer.getSequence(word.length()));
        }
        this.chunkedByteBuffer.unflip();
        int oldPosition = this.chunkedByteBuffer.position();
        this.chunkedByteBuffer.putSequence(word);
        this.chunkedByteBuffer.position(oldPosition);
        assertEquals(word, this.chunkedByteBuffer.getSequence(word.length()));
    }

}