module litehost.core {
    requires classindex;
    requires org.slf4j;
    exports com.litehost.core;
    exports com.litehost.core.connection;
    exports com.litehost.core.connection.transfer;
    exports com.litehost.core.connection.transfer.http;
}
