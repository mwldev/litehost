package com.litehost.core;

import com.litehost.core.connection.Connection;
import com.litehost.core.connection.Context;
import com.litehost.core.connection.transfer.Message;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class MessageHandler extends Thread {
    protected final Logger logger;
    protected final Selector selector; //this component only writes data, so no polymorphism is needed, therefore it uses good old plain Selector
    protected final Lock antiSpinLock;
    protected final Condition hasDelegations;
    protected volatile boolean running;


    protected MessageHandler(Logger logger, Selector selector) {
        this.logger = logger;
        this.selector = selector;
        this.antiSpinLock = new ReentrantLock(true);
        this.hasDelegations = this.antiSpinLock.newCondition();
    }

    public record Delegation(Connection connection, ByteBuffer data) {
    }

    public void delegate(Connection connection, Message message) {
        try {
            this.antiSpinLock.lock();
            Context context = Context.currentContext();
            ByteBuffer data = context.getProtocol()
                    .encoder()
                    .encode(message);
            Delegation delegation = new Delegation(connection, data);
            SocketChannel channel = delegation.connection().getChannel();
            SelectionKey key = channel.register(this.selector, SelectionKey.OP_WRITE);
            key.attach(delegation);
            this.hasDelegations.signal();
        } catch (ClosedChannelException e) {
            this.logger.error(e.getMessage());
        } finally {
            this.antiSpinLock.unlock();
        }
    }


    //todo implement
    public void requestStop() {

    }

    protected abstract void onStopRequest();

    protected boolean hasDelegations() {
        return this.selector.keys().isEmpty();
    }

}

