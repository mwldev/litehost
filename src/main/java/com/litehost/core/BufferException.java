package com.litehost.core;

public abstract class BufferException extends RuntimeException {
    public BufferException(String message) {
        super(message);
    }
}
