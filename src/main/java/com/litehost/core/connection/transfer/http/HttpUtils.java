package com.litehost.core.connection.transfer.http;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

class HttpUtils {
    private HttpUtils() {
    }

    public static String getMethod(String request) {
        return request.substring(0, request.indexOf(" "));
    }

    public static int pathLength(String path) {
        int length = 0;
        for (int i = 0; i < path.length(); i++) {
            if (path.charAt(i) == '/')
                ++length;
        }
        return length;
    }

    public static Map<String, String> getHeaders(String request) {
        Map<String, String> result = new HashMap<>();
        int blankLineIndex = getBlankLineIndex(request);
        request.substring(0, blankLineIndex).lines()
                .filter(line -> !line.contains("Cookie") && !line.contains("cookie"))
                .skip(1)
                .forEach(line -> {
                    int separator = line.indexOf(':');
                    if (separator != -1) {
                        result.put(line.substring(0, separator), line.substring(separator + 2));
                    }
                });
        return result;
    }

    private static Stream<String> cookieStream(String request) {
        return request.substring(0, getBlankLineIndex(request)).lines()
                .filter(line -> line.contains("Cookie") || line.contains("cookie"))
                .map(line -> line.substring(line.indexOf(":") + 2));
    }

    public static List<String> getCookies(String request) {
        List<String> cookies = new ArrayList<>();
        HttpUtils.cookieStream(request)
                .forEach(line -> {
                    String[] values = line.split(";");
                    cookies.addAll(Arrays.asList(values));
                });
        return cookies;
    }

    public static Map<String, String> cookieMap(String request) {
        Map<String, String> cookieMap = new HashMap<>();
        HttpUtils.cookieStream(request)
                .map(line -> line.split(";"))
                .forEach(pairs -> {
                    for (String pair : pairs) {
                        String[] data = pair.split("=");
                        cookieMap.put(data[0], data[1]);
                    }
                });
        return cookieMap;
    }

    public static String getStatusCode(String response) {
        int end = response.indexOf('\n');
        if (end == -1) {
            return response;
        }
        StringBuilder result = new StringBuilder();
        int start = response.indexOf(' ', response.indexOf(' ') + 1) + 1;
        for (int i = start; i < end; i++) {
            char character = response.charAt(i);
            if (character == ' ') {
                character = '_';
            }
            if (character == '\r') {
                continue;
            }
            result.append(Character.toUpperCase(character));
        }
        return result.toString();
    }

    public static String getResponseVersion(String response) {
        return response.substring(0, response.indexOf(' '));
    }

    public static Map<String, String> getHeaders(byte[] bytes) {
        String request = new String(bytes);
        return HttpUtils.getHeaders(request);
    }

    public static String getBody(String request) {
        int blankLineIndex = getBlankLineIndex(request);
        if (blankLineIndex == request.length())
            return null;
        return request.substring(blankLineIndex + 1);
    }

    public static String getBody(String request, int blankLineIndex) {
        int fromLine = blankLineIndex + 1;
        if (fromLine >= request.length())
            return null;
        return request.substring(fromLine);
    }

    public static HttpVersion getVersion(String request) {
        int firstSpace = request.indexOf(' ');
        int secondSpace = request.indexOf(' ', firstSpace + 1);
        int lastIndex = request.indexOf("\r\n");
        if (lastIndex == -1) {
            lastIndex = request.indexOf("\n");
        }
        String version = request.substring(secondSpace + 1, lastIndex);
        if (version.contains("1.1")) {
            return HttpVersion.HTTP_1_1;
        } else return HttpVersion.HTTP_2;
    }

    public static HttpRequest fromRawData(String request) {
        return HttpRequest.builder()
                .body(getBody(request))
                .version(getVersion(request))
                .headers(getHeaders(request))
                .method(HttpMethod.valueOf(getMethod(request)))
                .path(getResourcePath(request))
                .build();
    }

    public static HttpResponse parseRawData(String response) {
        return HttpResponse.builder()
                .status(HttpStatus.valueOf(getStatusCode(response)))
                .headers(getHeaders(response))
                .body(getBody(response))
                .build();
    }

    public static String getMethodAndPath(String request) {
        return getMethod(request) + getResourcePath(request);
    }

    public static String getResourcePath(String request) {
        int firstSpace = request.indexOf(" ") + 1;
        return request.substring(firstSpace, request.indexOf(" ", firstSpace + 1));
    }

    public static int getBlankLineIndex(String request) {
        int index = request.indexOf("\r\n\r\n");
        if (index == -1) {
            index = request.indexOf("\n\n");
        }
        if (index == -1) {
            return request.length();
        }
        return index + 3;
    }
}
