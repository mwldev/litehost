package com.litehost.core.connection.transfer;

import com.litehost.core.BufferOverflowException;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ChunkedByteBuffer extends MessageBuffer {
    private ChunkManager chunkManager;
    private final ByteBuffer conversionHelper;
    private final ByteOrder byteOrder;

    private ChunkedByteBuffer(
            int position, int limit,
            int capacity, ByteBuffer conversionHelper, ByteOrder byteOrder
    ) {
        super(position, limit, capacity);
        this.byteOrder = byteOrder;
        this.conversionHelper = conversionHelper;
    }

    protected ChunkedByteBuffer(int defaultCapacity, int chunkSize, ByteOrder byteOrder) {
        this(0, Integer.MAX_VALUE, Integer.MAX_VALUE, ByteBuffer.allocate(Long.BYTES), byteOrder);
        int chunkPoolSize = defaultCapacity / chunkSize; //good for now
        Queue<ByteBuffer> chunkPool = new ArrayDeque<>();
        for (int i = 0; i < chunkPoolSize; i++) {
            chunkPool.add(ChunkManager.makeChunk(chunkSize, byteOrder));
        }
        this.chunkManager = new ChunkManager(new LinkedList<>(), chunkPool, chunkSize, byteOrder);
    }


    private static class CopyUtils {

        public static void copy(ChunkedByteBuffer buffer, ByteBuffer bytes, int length, CopyDirection direction) {
            int currentPosition = buffer.position();
            buffer.shiftBy(length);
            int remaining = length;
            while (remaining > 0) {
                ByteBuffer chunk = buffer.chunkManager.chunk(currentPosition);
                int available = Math.min(chunk.remaining(), remaining);
                direction.copy(chunk, bytes, available);
                currentPosition += available;
                remaining -= available;
            }
        }

    }

    private enum CopyDirection {
        OUTSIDE {
            @Override
            public void copy(ByteBuffer buffer, ByteBuffer helper, int length) {
                helper.put(helper.position(), buffer, buffer.position(), length);
                this.shift(buffer, helper, length);
            }
        }, INSIDE {
            @Override
            public void copy(ByteBuffer buffer, ByteBuffer helper, int length) {
                buffer.put(buffer.position(), helper, helper.position(), length);
                this.shift(buffer, helper, length);
            }
        };

        public abstract void copy(ByteBuffer buffer, ByteBuffer helper, int length);

        protected final void shift(ByteBuffer buffer, ByteBuffer helper, int length) {
            this.shift(buffer, length);
            this.shift(helper, length);
        }

        private void shift(ByteBuffer buffer, int length) {
            buffer.position(buffer.position() + length);
        }

    }


    protected ChunkedByteBuffer(int defaultCapacity, int chunkSize) {
        this(defaultCapacity, chunkSize, ByteOrder.BIG_ENDIAN);
    }

    private void fromHelper(ByteBuffer helperBuffer) {
        int length = helperBuffer.remaining();
        CopyUtils.copy(this, helperBuffer, length, CopyDirection.INSIDE);
    }

    private void fromHelper() {
        this.fromHelper(this.conversionHelper);
    }

    private void toHelper(ByteBuffer helperBuffer, int bytes) {
        CopyUtils.copy(this, helperBuffer, bytes, CopyDirection.OUTSIDE);
    }

    private void toHelper(int bytes) {
        this.toHelper(this.conversionHelper, bytes);
    }

    public void trim() {
        this.position(this.chunkManager.trim());
    }

    @Override
    public void unflip() {
        super.unflip();
        this.chunkManager.unflip(this.position());
    }

    @Override
    public void clear() {
        super.clear();
        this.chunkManager.clear();
    }

    @Override
    public void flip() {
        super.flip();
        this.chunkManager.flip();
    }

    @Override
    public void put(byte bt) {
        this.chunkManager.chunk(this.position()).put(bt);
        this.shiftBy(1);
    }

    @Override
    public void put(ByteBuffer byteBuffer) {
        //todo implement
    }

    @Override
    public void putShort(short sh) {
        this.conversionHelper.clear()
                .putShort(sh)
                .flip();
        this.fromHelper();
    }

    @Override
    public void putInt(int i) {
        this.conversionHelper.clear()
                .putInt(i)
                .flip();
        this.fromHelper();
    }

    @Override
    public void putLong(long lg) {
        this.conversionHelper.clear()
                .putLong(lg)
                .flip();
        this.fromHelper();
    }

    @Override
    public void putFloat(float fl) {
        this.conversionHelper.clear()
                .putFloat(fl)
                .flip();
        this.fromHelper();
    }

    @Override
    public void putDouble(double db) {
        this.conversionHelper.clear()
                .putDouble(db)
                .flip();
        this.fromHelper();
    }

    @Override
    public void putSequence(CharSequence charSequence) {
        this.putSequence(charSequence, StandardCharsets.UTF_16);
    }

    @Override
    public void putSequence(CharSequence charSequence, Charset charset) {
        ByteBuffer bytes = ByteBuffer.wrap(
                charSequence.toString().getBytes(charset)
        ).order(this.byteOrder);
        this.fromHelper(bytes);
    }

    @Override
    public byte get() {
        byte bt = this.chunkManager.chunk(this.position()).get();
        this.shiftBy(1);
        return bt;
    }

    @Override
    public short getShort() {
        this.conversionHelper.clear();
        this.toHelper(Short.BYTES);
        return this.conversionHelper.flip().getShort();
    }

    @Override
    public int getInt() {
        this.conversionHelper.clear();
        this.toHelper(Integer.BYTES);
        return this.conversionHelper.flip().getInt();
    }

    @Override
    public long getLong() {
        this.conversionHelper.clear();
        this.toHelper(Long.BYTES);
        return this.conversionHelper.flip().getLong();
    }

    @Override
    public float getFloat() {
        this.conversionHelper.clear();
        this.toHelper(Float.BYTES);
        return this.conversionHelper.flip().getFloat();
    }

    @Override
    public double getDouble() {
        this.conversionHelper.clear();
        this.toHelper(Double.BYTES);
        return this.conversionHelper.flip().getDouble();
    }

    private CharSequence getSequence(
            Predicate<StringBuilder> stopCondition, Function<StringBuilder, CharSequence> formatter, Charset charset
    ) {
        CharsetDecoder decoder = charset.newDecoder();
        CharBuffer charBuffer = CharBuffer.allocate(256);
        ByteBuffer byteBuffer;
        int available;
        StringBuilder builder = new StringBuilder();
        do {
            if (this.hasRemaining()) {
                charBuffer.clear();
                byteBuffer = this.chunkManager.chunk(this.position());
                available = byteBuffer.remaining();
                decoder.decode(byteBuffer, charBuffer, false);
                int copied = available - byteBuffer.remaining();
                this.position(this.position() + copied);
                builder.append(charBuffer.flip());
            } else {
                throw new BufferOverflowException(
                        "Position: " + this.position() + ", limit: " + this.limit() + ", capacity: " + this.capacity()
                );
            }
        } while (!stopCondition.test(builder));
        CharSequence result = formatter.apply(builder);
        int resultByteCount = result.toString()                 //<----------------------------------------------------
                .getBytes(charset)                              // Nonoptimal af, should be refactored at some point
                .length;
        int total = builder.toString()
                .getBytes(charset)
                .length;                                        //<----------------------------------------------------
        int toRetrieve = total - resultByteCount;
        this.chunkManager.rewindBy(toRetrieve);
        this.position(this.position() - toRetrieve);
        return result;
    }

    @Override
    public CharSequence getSequence(int length) {
        return this.getSequence(length, StandardCharsets.UTF_16);
    }

    @Override
    public CharSequence getSequence(int length, Charset charset) {
        return this.getSequence(
                stringBuilder -> stringBuilder.length() >= length,
                stringBuilder -> stringBuilder.subSequence(0, length),
                charset
        );
    }

    @Override
    public CharSequence getSequence(String delimiter) {
        return this.getSequence(delimiter, StandardCharsets.UTF_16);
    }

    @Override
    public CharSequence getSequence(String delimiter, Charset charset) {
        return this.getSequence(
                new Predicate<>() {
                    private final Pattern pattern = Pattern.compile(delimiter);
                    private Matcher matcher = null;

                    @Override
                    public boolean test(StringBuilder stringBuilder) {
                        if (this.matcher == null) {
                            this.matcher = this.pattern.matcher(stringBuilder);
                        }
                        return this.matcher.find();
                    }
                },
                stringBuilder -> stringBuilder.subSequence(0, stringBuilder.indexOf(delimiter)),
                charset
        );
    }
}

class ChunkManager {
    private final List<ByteBuffer> chunks;
    private final Queue<ByteBuffer> chunkPool; // not at all sure about this particular solution, think about it
    private final ByteOrder byteOrder;
    private final int chunkSize;
    private final int chunkPoolSize;
    private int currentChunkIndex;

    private static record Coordinate(int position, int chunkIndex) {
    }

    public ChunkManager(List<ByteBuffer> chunks, Queue<ByteBuffer> chunkPool, int chunkSize, ByteOrder byteOrder) {
        this.chunks = chunks;
        this.chunkPool = chunkPool;
        this.chunkSize = chunkSize;
        this.chunkPoolSize = chunkPool.size();
        this.byteOrder = byteOrder;
        this.currentChunkIndex = 0;
    }

    private Coordinate positionToIndex(int position) {
        int inChunk = position % this.chunkSize;
        int chunkIndex = (position - inChunk) / this.chunkSize;
        return new Coordinate(inChunk, chunkIndex);
    }

    protected static ByteBuffer makeChunk(int chunkSize, ByteOrder byteOrder) {
        return ByteBuffer.allocate(chunkSize)
                .order(byteOrder);
    }

    private ByteBuffer makeChunk() {
        return makeChunk(this.chunkSize, this.byteOrder);
    }

    private int extend() {
        ByteBuffer chunk = this.chunkPool.poll();
        chunk = chunk == null ? this.makeChunk() : chunk;
        this.chunks.add(chunk);
        return chunk.position();
    }

    private ByteBuffer currentChunk() {
        return this.chunks.get(this.currentChunkIndex);
    }

    //Unflips buffer
    private void unflip(ByteBuffer buffer) {
        buffer.position(buffer.limit());
        buffer.limit(buffer.capacity());
    }

    //Removes unnecessary chunks
    public int trim() {
        int remainingInUse = this.currentChunkIndex;
        int toRetrieve = this.chunkPoolSize - this.chunkPool.size();
        while (remainingInUse > 0) {
            ByteBuffer chunk = this.chunks.remove(0);
            chunk.clear();
            this.chunkPool.add(chunk);
            remainingInUse--;
            toRetrieve--;
            this.currentChunkIndex--;
        }
        while (toRetrieve > 0) {
            this.chunkPool.add(this.makeChunk());
            toRetrieve--;
        }
        return this.currentChunk().position();
    }

    private void flip(Iterable<ByteBuffer> chunks) {
        for (ByteBuffer chunk : chunks) {
            chunk.flip();
        }
    }

    private void clear(Iterable<ByteBuffer> chunks) {
        for (ByteBuffer chunk : chunks) {
            chunk.clear();
        }
    }

    public void rewindBy(int bytes) {
        int toRetrieve = bytes;
        int newIndex = this.currentChunkIndex;
        for (int i = this.currentChunkIndex; toRetrieve > 0; i--) {
            ByteBuffer chunk = this.chunks.get(i);
            int inChunk = chunk.position();
            int retrieved = Math.min(toRetrieve, inChunk);
            int newPosition = inChunk - retrieved;
            toRetrieve -= retrieved;
            newIndex = i;
            chunk.position(newPosition);
        }
        this.currentChunkIndex = newIndex;
    }

    public void clear() {
        if (!this.chunks.isEmpty()) {
            this.trim();
            this.clear(this.chunks);
        }
    }

    public void flip() {
        this.flip(this.chunks);
        this.currentChunkIndex = 0;
    }


    public void unflip(int position) {
        this.currentChunkIndex = this.positionToIndex(position).chunkIndex();
        this.unflip(0, this.currentChunkIndex);
    }

    //Unflips chunks between given bounds, inclusive
    private void unflip(int lowerBound, int upperBound) {
        int limit = upperBound >= this.chunks.size() ? this.chunks.size() - 1 : upperBound;
        for (int i = lowerBound; i <= limit; i++) {
            ByteBuffer chunk = this.chunks.get(i);
            this.unflip(chunk);
        }
    }

    //Returns chunk corresponding to given position
    public ByteBuffer chunk(int position) {
        Coordinate coordinate = this.positionToIndex(position);
        this.currentChunkIndex = coordinate.chunkIndex();
        while (this.currentChunkIndex >= this.chunks.size()) {
            this.extend();
        }
        return this.currentChunk().position(coordinate.position());
    }
}
