package com.litehost.core.connection.transfer.http;

import java.util.HashMap;
import java.util.Map;

public class HttpResponse extends HttpMessage {
    private final HttpStatus status;

    public HttpResponse(HttpVersion version, Map<String, String> headers, String body, HttpStatus status) {
        super(version, headers, body);
        this.status = status;
    }

    public static Builder builder() {
        return new Builder();
    }

    public HttpStatus getStatus() {
        return this.status;
    }

    public static class Builder {
        private HttpVersion version;
        private HttpStatus status;
        private Map<String, String> headers;
        private String body;

        public Builder version(HttpVersion version) {
            this.version = version;
            return this;
        }

        public Builder status(HttpStatus status) {
            this.status = status;
            return this;
        }

        public Builder headers(Map<String, String> headers) {
            if (this.headers == null) {
                this.headers = headers;
            } else {
                this.headers.putAll(headers);
            }
            return this;
        }

        public Builder header(String name, String value) {
            if (this.headers == null) {
                this.headers = new HashMap<>();
            }
            this.headers.put(name, value);
            return this;
        }

        public Builder body(String body) {
            this.body = body;
            return this;
        }

        public HttpResponse build() {
            this.encode();
            return new HttpResponse(this.version, this.headers, this.body, this.status);
        }

        private void encode() {
            if (this.body != null) {
                this.headers.put("Content-Length", "" + this.body.length());
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(this.getVersion().toString()).append(' ')
                .append(this.status.toString())
                .append("\r\n");
        return this.parseHeaders(result).toString();
    }
}
