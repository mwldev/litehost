package com.litehost.core.connection.transfer;

import com.litehost.core.connection.transfer.http.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class MessageTransferProtocol<T extends Message> {
    private final MessageDecoder<T> messageDecoder;
    private final MessageEncoder<T> messageEncoder;
    public static final MessageTransferProtocol<HttpMessage> HTTP = new Http<>();

    public MessageTransferProtocol(MessageDecoder<T> messageDecoder, MessageEncoder<T> messageEncoder) {
        this.messageDecoder = messageDecoder;
        this.messageEncoder = messageEncoder;
    }

    public final MessageDecoder<T> decoder() {
        return this.messageDecoder;
    }

    public final MessageEncoder<T> encoder() {
        return this.messageEncoder;
    }

    public abstract T defaultErrorMessage();

    public abstract String getHandlingSignature(T message);


}

class Http<T extends HttpMessage> extends MessageTransferProtocol<T> { //total refactor required


    private static final Pattern bodyIndicatorPattern = Pattern.compile("([cC]ontent-[lL]ength:\\s?\\d+ ?)|([tT]ransfer-[eE]ncoding:\\s?(\\w+,?\\s?)+\\s?)");
    private static final Pattern trailingChunkPattern = Pattern.compile("0\r\n.*?\r\n");
    private static final Pattern chunkStartPattern = Pattern.compile("[1-9A-F]+\r\n");

    public Http() {
        super(new HttpMessageDecoder<>(), new HttpMessageEncoder<>());
    }


    public boolean messageComplete(CharSequence message) {
        boolean headersComplete = this.headersComplete((StringBuilder) message);
        if (!headersComplete) {
            return false;
        } else {
            StringBuilder result = (StringBuilder) message;
            int blankLine = result.indexOf("\r\n\r\n") + 3;
            String bodyIndicator = this.bodyIndicator(result);
            if (bodyIndicator == null) {
                return true;
            } else {
                int separator = bodyIndicator.indexOf(":");
                return this.bodyComplete(result, blankLine, separator, bodyIndicator);
            }
        }
    }

    public boolean headersComplete(StringBuilder result) {
        return result.indexOf("\r\n\r\n") != -1;
    }

    public String bodyIndicator(StringBuilder result) {
        Matcher matcher = bodyIndicatorPattern.matcher(result);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return null;
        }
    }

    public boolean bodyComplete(StringBuilder result, int blankLine, int separator, String header) {
        if (header.contains("ransfer")) {
            Matcher matcher = trailingChunkPattern.matcher(result);
            boolean complete = matcher.find();
            if (complete) {
                String last = matcher.group();
                result.replace(result.indexOf(last), result.length(), last);
                this.decode(result, blankLine);
            }
            return complete;
        } else if (header.contains("ontent")) {
            int givenContentLength;
            if (header.charAt(separator + 1) == ' ') {
                givenContentLength = Integer.parseInt(header.substring(separator + 2));
            } else {
                givenContentLength = Integer.parseInt(header.substring(separator + 1));
            }
            return result.length() - (blankLine + 1) == givenContentLength;
        } else return false;
    }

    public void decode(StringBuilder result, int blankLine) {
        StringBuilder newBody = new StringBuilder();
        Matcher matcher = chunkStartPattern.matcher(result);
        matcher.region(blankLine + 1, result.length());
        while (matcher.find()) {
            int chars = matcher.start();
            int start = chars + 3;
            int end = start + this.getHexValue(result.charAt(chars));
            newBody.append(result, start, end);
        }
        result.replace(blankLine + 1, result.length(), newBody.toString());
    }

    public int getHexValue(char c) {
        if (c < 'A') {
            return c - 48;
        } else return c - 55;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T defaultErrorMessage() {
        return (T) HttpResponse.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .build();
    }

    @Override
    public String getHandlingSignature(T message) {
        HttpRequest request = (HttpRequest) message; // only requests need handling signatures, we don't care about response for now
        return request.getMethod() + request.getPath();
    }
}
