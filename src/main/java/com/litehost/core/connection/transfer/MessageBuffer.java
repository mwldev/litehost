package com.litehost.core.connection.transfer;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public abstract class MessageBuffer {
    private int position;
    private int limit;
    private final int capacity; // Making this a variable reference could reduce some code in the implementation. On the other hand, this way it looks as if I was smart and knew what's going on

    protected MessageBuffer(int position, int limit, int capacity) {
        this.position = position;
        this.limit = limit;
        this.capacity = capacity;
    }

    public static MessageBuffer create(int capacity) {
        return new ChunkedByteBuffer(capacity, 256);
    }

    public int position() {
        return this.position;
    }

    public void position(int position) {
        if (position <= this.limit && position >= 0) {
            this.position = position;
        } else {
            throw new IllegalArgumentException(); //all exceptions are temporary, requires some neat custom exceptions
        }
    }

    public int remaining() { //override !!!!
        return this.limit - this.position;
    }

    public boolean hasRemaining() { //override!!!
        return this.remaining() > 0;
    }

    public int limit() {
        return this.limit;
    }

    public void limit(int limit) {
        if (limit <= this.capacity && limit >= 0) {
            this.limit = limit;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public int capacity() {
        return this.capacity;
    }

    public void rewind() {
        this.position(0);
    }

    public void flip() {
        this.limit(this.position);
        this.position(0);
    }

    public void unflip() {
        this.position(this.limit);
        this.limit(this.capacity);
    }

    public void clear() {
        this.position(0);
        this.limit(this.capacity);
    }

    protected final void shiftBy(int bytes) {
        this.position(this.position + bytes);
    }

    public abstract void put(byte bt);

    public abstract void put(ByteBuffer byteBuffer);

    public abstract void putShort(short sh);

    public abstract void putInt(int i);

    public abstract void putLong(long lg);

    public abstract void putFloat(float fl);

    public abstract void putDouble(double db);

    public abstract void putSequence(CharSequence charSequence);

    public abstract void putSequence(CharSequence charSequence, Charset charset);

    public abstract byte get();

    public abstract short getShort();

    public abstract int getInt();

    public abstract long getLong();

    public abstract float getFloat();

    public abstract double getDouble();

    public abstract CharSequence getSequence(int length);

    public abstract CharSequence getSequence(int length, Charset charset);

    public abstract CharSequence getSequence(String delimiter);

    public abstract CharSequence getSequence(String delimiter, Charset charset);

}
