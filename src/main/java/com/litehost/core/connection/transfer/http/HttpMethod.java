package com.litehost.core.connection.transfer.http;

public enum HttpMethod {
    GET, POST, PUT, PATCH, DELETE, HEAD, CONNECT, OPTIONS, TRACE;

    @Override
    public String toString() {
        return super.toString();
    }
}
