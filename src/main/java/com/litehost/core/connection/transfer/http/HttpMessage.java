package com.litehost.core.connection.transfer.http;

import com.litehost.core.connection.transfer.Message;

import java.util.Map;

public abstract class HttpMessage implements Message {
    private final HttpVersion version;
    private final Map<String, String> headers;
    private final String body;

    protected HttpMessage(HttpVersion version, Map<String, String> headers, String body) {
        this.version = version;
        this.headers = headers;
        this.body = body;
    }

    public HttpVersion getVersion() {
        return this.version;
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public String getBody() {
        return this.body;
    }

    protected final StringBuilder parseHeaders(StringBuilder result) {
        if (this.headers != null) {
            this.headers.forEach((key, value) -> {
                result.append(key)
                        .append(": ")
                        .append(value)
                        .append("\r\n");
            });
        }
        result.append("\r\n");
        if (this.body != null) {
            result.append(this.body);
        }
        return result;
    }
}
