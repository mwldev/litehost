package com.litehost.core.connection.transfer.http;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class HttpRequest extends HttpMessage {
    private final String method;
    private final String path;

    protected HttpRequest(HttpVersion version, Map<String, String> headers, String body, String method, String path) {
        super(version, headers, body);
        this.method = method;
        this.path = path;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private HttpVersion version;
        private Map<String, String> headers;
        private String body;
        private String method;
        private String path;

        public Builder version(HttpVersion version) {
            this.version = version;
            return this;
        }

        public Builder URL(String url) {
            if (this.path == null || this.headers.get("Host") == null) {
                URI uri = URI.create(url);
                this.path = uri.getPath();
                this.header("Host", uri.getHost());
            }
            return this;
        }


        public Builder header(String header, String value) {
            if (this.headers != null)
                this.headers.put(header, value);
            else this.headers = new HashMap<>() {{
                put(header, value);
            }};
            return this;
        }

        public Builder host(String host) {
            return this.header("Host", host);
        }

        public Builder headers(Map<String, String> headers) {
            if (this.headers == null) {
                this.headers = headers;
            } else {
                this.headers.putAll(headers);
            }
            return this;
        }

        public Builder body(String body) {
            this.body = body;
            return this;
        }

        public Builder method(HttpMethod method) {
            this.method = method.toString();
            return this;
        }

        public Builder path(String path) {
            this.path = path;
            return this;
        }

        private void encode() {
            this.headers.put("Content-Length", "" + this.body.length());
        }

        public HttpRequest build() {
            if (this.headers == null) {
                throw new IllegalStateException("Cannot create request without headers!");
            } else if (this.method == null) {
                throw new IllegalStateException("Missing method!");
            } else if (this.path == null) {
                throw new IllegalStateException("Missing resource path!");
            }
            if (this.body != null) {
                this.encode();
            }
            return new HttpRequest(this.version, this.headers, this.body, this.method, this.path);
        }


    }

    public String getMethod() {
        return this.method;
    }

    public String getPath() {
        return this.path;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(this.method)
                .append(' ')
                .append(this.path)
                .append(' ')
                .append(this.getVersion().toString())
                .append("\r\n");
        return this.parseHeaders(result).toString();
    }

}