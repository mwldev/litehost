package com.litehost.core.connection.transfer.http;

public enum HttpVersion {
    HTTP_1_1("HTTP/1.1"), HTTP_2("HTTP/2");

    HttpVersion(String description) {
        this.description = description;
    }

    private String description;

    @Override
    public String toString() {
        return this.description;
    }
}
