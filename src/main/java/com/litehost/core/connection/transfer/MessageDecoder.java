package com.litehost.core.connection.transfer;

public interface MessageDecoder<T extends Message> {
    T decode(MessageBuffer input);
}
