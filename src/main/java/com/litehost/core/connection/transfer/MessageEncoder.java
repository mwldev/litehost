package com.litehost.core.connection.transfer;

import java.nio.ByteBuffer;

public interface MessageEncoder<T extends Message> {
    ByteBuffer encode(T message);
}
