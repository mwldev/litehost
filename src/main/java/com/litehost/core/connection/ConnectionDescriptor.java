package com.litehost.core.connection;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class ConnectionDescriptor extends Descriptor<Connection, Context> {
    private final InetSocketAddress remoteAddress;
    private final Context context;

    ConnectionDescriptor(
            InetSocketAddress localAddress, SelectionKey selectionKey,
            InetSocketAddress remoteAddress, Context context
    ) {
        super(localAddress, selectionKey);
        this.remoteAddress = remoteAddress;
        this.context = context;
    }

    public Context context() {
        return this.context;
    }

    @Override
    public Connection connectable() {
        return super.connectable();
    }

    @Override
    public Descriptor<Connection, Context> setConnectable(Connection connectable) {
        super.setConnectable(connectable);
        return null;
    }

    public InetSocketAddress remoteAddress() {
        return this.remoteAddress;
    }
}
