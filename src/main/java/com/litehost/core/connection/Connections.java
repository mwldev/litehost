package com.litehost.core.connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

class Connections {

    @SuppressWarnings("unchecked") //encapsulates necessary, but not so good-looking casts
    public static <T extends Connectable, S> Descriptor<T, S> get(SelectionKey key) {
        Descriptor<T, S> result;
        if (key != null) {
            result = ((Descriptor<T, S>) key.attachment());
        } else {
            throw new NotRegisteredException("Connectable not registered");
        }
        return result;
    }

    public static ConnectionDescriptor registerConnection(SelectionKey selectionKey, Context context, Connection connection) throws IOException {
        return (ConnectionDescriptor) Descriptor.newDescriptor(
                        (InetSocketAddress) connection.getChannel().getLocalAddress(),
                        selectionKey, (InetSocketAddress) connection.getChannel().getRemoteAddress(),
                        context)
                .setConnectable(connection);
    }

    public static ConnectionDescriptor newConnection(
            SelectionKey key, Context context,
            InetSocketAddress remoteAddress, InetSocketAddress localAddress,
            ConnectionAddressResolver addressResolver
    ) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        addressResolver.register(key, remoteAddress);
        return (ConnectionDescriptor) Descriptor.newDescriptor(
                        localAddress, key, remoteAddress, context
                )
                .setConnectable(Connection.createNew(channel));
    }

    public static ConnectionAcceptorDescriptor newAcceptor(
            SelectionKey selectionKey, InetSocketAddress address,
            ConnectionController connectionController
    ) throws IOException {
        return (ConnectionAcceptorDescriptor) Descriptor.newDescriptor(address, selectionKey)
                .setConnectable(
                        ConnectionAcceptor.createNew(
                                (ServerSocketChannel) selectionKey.channel(),
                                connectionController
                        ));
    }

    public static SocketChannel newNonBlockingChannel() throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        return socketChannel;
    }


    public static SocketChannel newBoundChannel(
            InetSocketAddress remoteAddress, InetSocketAddress localAddress
    ) throws IOException {
        SocketChannel channel = Connections.newNonBlockingChannel();
        channel.connect(remoteAddress);
        channel.bind(localAddress);
        return channel;
    }

}
