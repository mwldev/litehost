package com.litehost.core.connection;

public class ConnectableException extends RuntimeException {
    public ConnectableException(String message) {
        super(message);
    }
}
