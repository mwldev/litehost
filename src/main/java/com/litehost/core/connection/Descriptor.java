package com.litehost.core.connection;

import com.litehost.core.dispatch.OperationType;

import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;

public class Descriptor<T extends Connectable, S> {
    private T connectable;
    private final InetSocketAddress localAddress;
    private final SelectionKey selectionKey;

    protected Descriptor(InetSocketAddress localAddress, SelectionKey selectionKey) {
        this.localAddress = localAddress;
        this.selectionKey = selectionKey;
        selectionKey.attach(this);
    }

    public InetSocketAddress localAddress() {
        return this.localAddress;
    }

    public Descriptor<T, S> setConnectable(T connectable) {
        this.connectable = connectable;
        return this;
    }

    public T connectable() {
        return this.connectable;
    }

    static ConnectionAcceptorDescriptor newDescriptor(InetSocketAddress localAddress, SelectionKey selectionKey) {
        return new ConnectionAcceptorDescriptor(localAddress, selectionKey);
    }

    static ConnectionDescriptor newDescriptor(
            InetSocketAddress localAddress, SelectionKey selectionKey,
            InetSocketAddress remoteAddress, Context context
    ) {
        return new ConnectionDescriptor(localAddress, selectionKey, remoteAddress, context);
    }
    // ^ this looks worse than me, dry it or create a distinct factory

    SelectionKey key() {
        return this.selectionKey;
    }

    public void changeInterest(OperationType operation) {
        this.selectionKey.interestOps(operation.mask());
    }

    public OperationType interestOps() {
        return OperationType.fromMask(this.selectionKey.readyOps());
    }

    public OperationType readyOps() {
        return OperationType.fromMask(this.selectionKey.readyOps());
    }
}
