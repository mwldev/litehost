package com.litehost.core.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Connection implements Closeable, Connectable {
    private final SocketChannel channel;
    private final ByteBuffer inputBuffer;
    private final Logger logger;

    private Connection(SocketChannel channel, ByteBuffer inputBuffer, Logger logger) {
        this.channel = channel;
        this.inputBuffer = inputBuffer;
        this.logger = logger;
    }

    protected static Connection createNew(SocketChannel channel) throws IOException {
        return Connection.createNew(channel, 4 * 1024);
    }

    protected static Connection createNew(SocketChannel channel, int bufferSize) throws IOException {
        channel.setOption(StandardSocketOptions.SO_SNDBUF, bufferSize)
                .setOption(StandardSocketOptions.SO_RCVBUF, bufferSize);
        return new Connection(
                channel, ByteBuffer.allocateDirect(bufferSize),
                LoggerFactory.getLogger(Connection.class)
        );
    }

    @Override
    public SocketChannel getChannel() {
        return this.channel;
    }

    public int getBufferSize() {
        return this.inputBuffer.capacity();
    }

    public int write(ByteBuffer buffer) {
        try {
            this.channel.write(buffer);
        } catch (IOException e) {
            this.logger.error(e.getMessage());
        }
        return buffer.remaining();
    }

    public ByteBuffer read() {
        try {
            this.channel.read(this.inputBuffer);
        } catch (IOException e) {
            this.logger.error(e.getMessage());
        }
        return this.inputBuffer.flip();
    }

    @Override
    public void close() throws IOException {
        this.channel.close();
    }

}

