package com.litehost.core.connection;

import com.litehost.core.MessageHandler;
import com.litehost.core.connection.transfer.Message;
import com.litehost.core.connection.transfer.MessageBuffer;
import com.litehost.core.connection.transfer.MessageTransferProtocol;
import com.litehost.core.dispatch.Session;
import com.litehost.core.dispatch.Task;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public abstract sealed class Context implements Closeable permits Session {
    private final MessageBuffer input;
    private final MessageHandler messageHandler;
    private final InetSocketAddress remoteAddress;
    private final ExecutorService taskExecutor;
    private MessageTransferProtocol<? super Message> protocol;

    protected Context(
            MessageBuffer input, MessageHandler messageHandler, InetSocketAddress remoteAddress,
            ExecutorService taskExecutor,
            MessageTransferProtocol<? super Message> protocol
    ) {
        this.input = input;
        this.messageHandler = messageHandler;
        this.remoteAddress = remoteAddress;
        this.taskExecutor = taskExecutor;
        this.protocol = protocol;
    }

    public MessageBuffer getInput() {
        return this.input;
    }

    public MessageHandler getMessageHandler() {
        return this.messageHandler;
    }

    public SocketAddress getRemoteAddress() {
        return this.remoteAddress;
    }

    public ExecutorService getTaskExecutor() {
        return this.taskExecutor;
    }

    public MessageTransferProtocol<? super Message> getProtocol() {
        return this.protocol;
    }

    public void setProtocol(MessageTransferProtocol<? super Message> protocol) {
        this.protocol = protocol;
    }

    //Some would say this both looks bad and works bad, but I like having fun with reflection.
    //This also looks super cool, as if I was actually smart and had refined skills in Java.
    public static Context currentContext() {
        try {
            Field runnableField = Thread.class.getDeclaredField("target");
            try {
                runnableField.setAccessible(true);
                if (runnableField.get(Thread.currentThread()) instanceof Task currentTask) {
                    return currentTask.context();
                } else {
                    throw new IllegalStateException("This thread does not run in the container's Context");
                }
            } finally {
                runnableField.setAccessible(false);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new IllegalStateException("This thread does not run in the container's Context");
        }
    }

    public abstract void onDataReceived(Connection connection);

    public abstract void onFinishedTask(Message result);

    @Override
    public void close() throws IOException {
        //todo implement
    }
}
