package com.litehost.core.connection;

import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class ConnectionAddressResolver { // could have a better implementation, but will work for now
    private final Map<InetSocketAddress, SelectionKey> remoteAddressMapping;

    public static ConnectionAddressResolver newAddressResolver() {
        return new ConnectionAddressResolver(new HashMap<>());
    }

    ConnectionAddressResolver(Map<InetSocketAddress, SelectionKey> remoteAddressMapping) {
        this.remoteAddressMapping = remoteAddressMapping;
    }

    public void register(SelectionKey key, InetSocketAddress address) {
        this.remoteAddressMapping.put(address, key);
    }

    public Optional<SelectionKey> resolveRemote(InetSocketAddress remoteAddress) {
        return Optional.ofNullable(this.remoteAddressMapping.get(remoteAddress));
    }

    public void remove(InetSocketAddress address) {
        this.remoteAddressMapping.remove(address);
    }

}
