package com.litehost.core.connection;

import com.litehost.core.connection.ConnectableException;

class NotRegisteredException extends ConnectableException {
    public NotRegisteredException(String message) {
        super(message);
    }
}
