package com.litehost.core.connection;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class ConnectionAcceptor implements Closeable, Connectable {
    private final ServerSocketChannel serverSocketChannel;
    private final ConnectionController connectionController;


    private ConnectionAcceptor(ServerSocketChannel serverSocketChannel, ConnectionController connectionController) throws IOException {
        this.serverSocketChannel = serverSocketChannel;
        this.connectionController = connectionController;
        this.serverSocketChannel.configureBlocking(false);
    }

    public static ConnectionAcceptor createNew(
            ServerSocketChannel serverSocketChannel, ConnectionController connectionController
    ) throws IOException {

        return new ConnectionAcceptor(serverSocketChannel, connectionController);
    }

    @Override
    public SelectableChannel getChannel() {
        return this.serverSocketChannel;
    }

    public Connection accept() throws IOException {
        SocketChannel channel = this.serverSocketChannel.accept();
        channel.configureBlocking(false);
        return Connection.createNew(channel);

    }

    @Override
    public void close() throws IOException {
        this.serverSocketChannel.close();
    }
}
