package com.litehost.core.connection;

import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;

public class ConnectionAcceptorDescriptor extends Descriptor<ConnectionAcceptor, Void> {
    ConnectionAcceptorDescriptor(InetSocketAddress localAddress, SelectionKey selectionKey) {
        super(localAddress, selectionKey);
    }

    @Override
    public ConnectionAcceptor connectable() {
        return super.connectable();
    }

    @Override
    public Descriptor<ConnectionAcceptor, Void> setConnectable(ConnectionAcceptor connectable) {
        return super.setConnectable(connectable);
    }
}
