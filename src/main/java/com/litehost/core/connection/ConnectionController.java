package com.litehost.core.connection;

import com.litehost.core.dispatch.OperationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public final class ConnectionController implements Closeable, Iterable<Descriptor<? extends Connectable, ?>> {
    private final Selector selector;
    private final InetAddress localBindAddress;
    private final Logger logger;
    private final ConnectionAddressResolver addressResolver;

    private static class ConnectableIterator implements Iterator<Descriptor<? extends Connectable, ?>> {
        private final Iterator<SelectionKey> internalIterator;

        private ConnectableIterator(Iterator<SelectionKey> internalIterator) {
            this.internalIterator = internalIterator;
        }

        @Override
        public boolean hasNext() {
            return this.internalIterator.hasNext();
        }

        @Override
        public Descriptor<? extends Connectable, ?> next() {
            return Connections.get(this.internalIterator.next());
        }

        @Override
        public void remove() {
            this.internalIterator.remove();
        }

        @Override
        public void forEachRemaining(Consumer<? super Descriptor<? extends Connectable, ?>> action) {
            Iterator.super.forEachRemaining(action); //maybe fine?
        }
    }

    private ConnectionController(Selector selector, InetAddress localBindAddress, Logger logger, ConnectionAddressResolver addressResolver) {
        this.selector = selector;
        this.localBindAddress = localBindAddress;
        this.logger = logger;
        this.addressResolver = addressResolver;
    }

    public static ConnectionController newController(Selector selector, InetAddress localBindAddress, Logger logger) {
        return new ConnectionController(
                selector, localBindAddress, logger, ConnectionAddressResolver.newAddressResolver()
        );
    }

    public static ConnectionController newController(Selector selector, InetAddress localBindAddress) {
        return ConnectionController.newController(
                selector, localBindAddress, LoggerFactory.getLogger(ConnectionController.class)
        );
    }


    private ConnectionDescriptor startConnection(int localPort, InetSocketAddress remoteAddress, Context context) throws IOException {
        InetSocketAddress localAddress = new InetSocketAddress(this.localBindAddress, localPort);
        SelectionKey key = this.registerChannel(
                Connections.newBoundChannel(remoteAddress, localAddress),
                SelectionKey.OP_CONNECT
        );
        return Connections.newConnection(
                key, context, remoteAddress,
                localAddress, this.addressResolver
        );
    }

    SelectionKey registerChannel(SelectableChannel channel, int ops) throws ClosedChannelException {
        return channel.register(this.selector, ops);
    }

    public Selector selector() {
        return this.selector;
    }


    public void stopConnection(ConnectionDescriptor descriptor) {
        InetSocketAddress remoteAddress = descriptor.remoteAddress();
        Optional<SelectionKey> key = this.addressResolver.resolveRemote(remoteAddress);
        key.ifPresent((selectionKey -> {
            try {
                this.addressResolver.remove(remoteAddress);
                selectionKey.channel().close();
            } catch (IOException e) {
                this.logger.error(e.getMessage());
            }
        }));
    }

    public InetAddress getLocalBindAddress() {
        return this.localBindAddress;
    }

    ConnectionAddressResolver getAddressResolver() {
        return this.addressResolver;
    }

    public ConnectionAcceptorDescriptor openAcceptor(int localPort) throws IOException {
        return Connections.newAcceptor(
                this.registerChannel(ServerSocketChannel.open(), SelectionKey.OP_ACCEPT),
                new InetSocketAddress(this.localBindAddress, localPort),
                this
        );
    }

    public ConnectionDescriptor register(Connection connection, Context context, OperationType operationType) throws IOException {
        SocketChannel channel = connection.getChannel();
        SelectionKey selectionKey = this.registerChannel(channel, operationType.mask());
        return Connections.registerConnection(selectionKey, context, connection);
    }

    public ConnectionDescriptor openConnection(
            int localPort, InetSocketAddress remoteAddress, Context context
    ) throws IOException {
        return this.startConnection(localPort, remoteAddress, context);
    }

    public ConnectionController awaitReady() throws IOException { // needs to be a bit better, probably another set is necessary
        this.selector.select();
        return this;
    }

    @Override
    public void close() throws IOException {
        this.selector.close();
    }

    @Override
    public Iterator<Descriptor<? extends Connectable, ?>> iterator() {
        return new ConnectableIterator(this.selector.selectedKeys().iterator());
    }

    @Override
    public void forEach(Consumer<? super Descriptor<? extends Connectable, ?>> action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator<Descriptor<? extends Connectable, ?>> spliterator() {
        return Iterable.super.spliterator();
    }
}

