package com.litehost.core.connection;

import java.nio.channels.SelectableChannel;

public interface Connectable {
    SelectableChannel getChannel();
}
