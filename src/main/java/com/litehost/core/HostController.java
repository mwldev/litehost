package com.litehost.core;

import com.litehost.core.connection.ConnectionController;
import com.litehost.core.connection.transfer.Message;
import com.litehost.core.connection.transfer.MessageTransferProtocol;
import com.litehost.core.dispatch.ExceptionHandlers;
import com.litehost.core.dispatch.MessageWriter;
import com.litehost.core.dispatch.SessionFactory;
import com.litehost.core.dispatch.TaskDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.Selector;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

public class HostController {
    private static HostController controller;
    private static final Logger logger = LoggerFactory.getLogger(HostController.class);
    private final InetAddress localAddress;
    private final TaskDispatcher taskDispatcher;

    private HostController(
            MessageWriter dataWriter, ExecutorService executorService,
            InetAddress address,
            Map<String, MessageTransferProtocol<? super Message>> protocols,
            ConnectionController controller
    ) throws IOException {
        this.localAddress = address;
        this.taskDispatcher = new TaskDispatcher(
                SessionFactory.newFactory(dataWriter, executorService, protocols),
                LoggerFactory.getLogger(TaskDispatcher.class), controller
        );
    }

    public static HostController configure(InetAddress address, int minThreadPool, int maxThreadPool) {
        if (HostController.controller == null) {
            try {
                Logger dataWriterLogger = LoggerFactory.getLogger(MessageWriter.class);
                Selector selector = Selector.open();
                MessageWriter dataWriter = new MessageWriter(dataWriterLogger, selector);
                ExecutorService executorService = new ThreadPoolExecutor(minThreadPool, maxThreadPool,
                        5, TimeUnit.SECONDS, new LinkedBlockingQueue<>(),
                        runnable -> {
                            Thread thread = new Thread(runnable);
                            thread.setDaemon(false);
                            thread.setUncaughtExceptionHandler(ExceptionHandlers.logHandler());
                            return thread;
                        });
                Logger logger = LoggerFactory.getLogger(TaskDispatcher.class);
                ConnectionController controller = ConnectionController.newController(Selector.open(), address);
                HostController.controller = new HostController(
                        dataWriter, executorService,
                        address,
                        new HashMap<>(), controller
                );
            } catch (IOException e) {
                HostController.logger.error(e.getMessage());
                System.exit(420);
            }
        } else {
            throw new IllegalStateException(); // temporary
        }
        return HostController.controller;
    }

    public static HostController configure(InetAddress address, int maxThreadPool) {
        return HostController.configure(address, 1, maxThreadPool);
    }

    public InetAddress getLocalAddress() {
        return localAddress;
    }

    public static HostController configure(InetAddress address) {
        return HostController.configure(address, 5, 20);
    }

    public static HostController controller() {
        return HostController.controller;
    }

    public Message sendMessage(String address, int port, Message message) {
        InetSocketAddress inetSocketAddress = new InetSocketAddress(address, port);
        return this.send(inetSocketAddress, message);
    }

    public Message sendMessage(String address, Message message) {
        InetSocketAddress inetSocketAddress = new InetSocketAddress(address, 80);
        return this.send(inetSocketAddress, message);
    }

    private Message send(InetSocketAddress address, Message message) {
        //needs another abstraction layer, that is some kind of connection manager, with embedded Selector and a Map of all connections
        return null; //todo implement
    }

}
