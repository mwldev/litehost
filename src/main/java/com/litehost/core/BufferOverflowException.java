package com.litehost.core;

public class BufferOverflowException extends BufferException {
    public BufferOverflowException(String message) {
        super(message);
    }
}
