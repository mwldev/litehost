package com.litehost.core.annotate;

import com.litehost.core.connection.transfer.Message;
import com.litehost.core.connection.transfer.MessageDecoder;
import org.atteo.classindex.IndexAnnotated;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@IndexAnnotated
public @interface Protocol {
    int port() default 80;
}
