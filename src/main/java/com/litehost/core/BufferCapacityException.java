package com.litehost.core;

public abstract class BufferCapacityException extends BufferException {
    public BufferCapacityException(String message) {
        super(message);
    }
}
