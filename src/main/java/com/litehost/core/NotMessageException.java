package com.litehost.core;

public class NotMessageException extends RuntimeException {
    public NotMessageException(String msg) {
        super(msg);
    }
}
