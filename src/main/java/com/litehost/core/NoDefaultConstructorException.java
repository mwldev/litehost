package com.litehost.core;

public class NoDefaultConstructorException extends RuntimeException {
    public NoDefaultConstructorException(String msg) {
        super(msg);
    }
}
