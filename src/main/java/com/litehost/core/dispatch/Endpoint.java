package com.litehost.core.dispatch;

import java.lang.reflect.Method;

public record Endpoint(Path path, Method handler, Object instance) {
}
