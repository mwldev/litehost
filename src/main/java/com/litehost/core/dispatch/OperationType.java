package com.litehost.core.dispatch;

import java.nio.channels.SelectionKey;

public enum OperationType { //translation from SelectionKey.OP_X bitmasks
    ACCEPT(0, SelectionKey.OP_ACCEPT),
    READ(1, SelectionKey.OP_READ),
    WRITE(2, SelectionKey.OP_WRITE),
    READ_WRITE(3, SelectionKey.OP_READ | SelectionKey.OP_WRITE),
    CONNECT(4, SelectionKey.OP_CONNECT),
    NOP(5, 0); // not sure about this mask

    private final int code;
    private final int mask;
    private static final OperationType[] DECODER = OperationType.values();

    OperationType(int code, int mask) {
        this.code = code;
        this.mask = mask;
    }

    private static int removeBit(int value, int index) {
        int mask = -1 << index;
        return (value & ~mask) | ((value >>> 1) & mask);
    }

    private static int trimOperationSignature(int value) {
        return removeBit(value, 1) % 8; // modulus of 8 since after bit removal max value is 8
    }

    public static OperationType fromCode(int code) {
        return DECODER[code];
    }

    public static OperationType fromMask(int bitmask) {
        return fromCode(trimOperationSignature(bitmask));
    }

    public int mask() {
        return this.mask;
    }

    public int code() {
        return this.code;
    }

}

