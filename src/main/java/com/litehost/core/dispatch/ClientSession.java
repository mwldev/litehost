package com.litehost.core.dispatch;

import com.litehost.core.MessageHandler;
import com.litehost.core.connection.Connection;
import com.litehost.core.connection.transfer.Message;
import com.litehost.core.connection.transfer.MessageBuffer;
import com.litehost.core.connection.transfer.MessageTransferProtocol;
import org.slf4j.Logger;

import java.net.InetSocketAddress;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public non-sealed class ClientSession extends Session {

    protected ClientSession(
            MessageBuffer input, MessageHandler messageHandler,
            InetSocketAddress remoteAddress, ExecutorService taskExecutor,
            MessageTransferProtocol<? super Message> protocol,
            LocalDateTime startTime, Logger logger, long timeout
    ) {
        super(
                input, messageHandler, remoteAddress,
                taskExecutor, protocol, startTime,
                logger, timeout
        );
    }

    @Override
    public void onDataReceived(Connection connection) {

    }

    @Override
    public void onFinishedTask(Message result) {
    }
}
