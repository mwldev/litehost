package com.litehost.core.dispatch;

import com.litehost.core.MessageHandler;
import com.litehost.core.connection.Context;
import com.litehost.core.connection.transfer.Message;
import com.litehost.core.connection.transfer.MessageTransferProtocol;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

public class Task implements Runnable {
    private final Context context;
    private final MessageHandler messageHandler;
    private final Message message;
    private final String handlingSignature;
    private final Map<Path, Endpoint> requestMappings;
    private final PathResolver pathResolver;
    private final MessageTransferProtocol<? super Message> protocol;

    public Task(
            Context context, MessageHandler messageHandler, Message message, String handlingSignature,
            Map<Path, Endpoint> requestMappings, PathResolver pathResolver
    ) {
        this.context = context;
        this.messageHandler = messageHandler;
        this.message = message;
        this.handlingSignature = handlingSignature;
        this.requestMappings = requestMappings;
        this.pathResolver = pathResolver;
        this.protocol = context.getProtocol();
    }

    private Message callHandlingMethod()
            throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Endpoint endpoint = this.requestMappings.get(handlingSignature);
        Map<String, Object> variables = new HashMap<>();
        if (endpoint == null) {
            Path path = this.pathResolver.resolveGeneric(handlingSignature, variables);
            endpoint = this.requestMappings.get(path);
            if (endpoint == null) {
                return this.protocol.defaultErrorMessage();
            }
        }
        Object instance = endpoint.instance();
        Method handlingMethod = endpoint.handler();
        Parameter[] parameters = handlingMethod.getParameters();
        return switch (parameters.length) {
            case 0 -> (Message) handlingMethod.invoke(instance);
            case 1 -> (Message) handlingMethod.invoke(instance, this.message);
            case 2 -> (Message) handlingMethod.invoke(instance, this.message, variables);
            default -> this.protocol.defaultErrorMessage();
        };
    }

    public Context context() {
        return this.context;
    }

    @Override
    public void run() {
        Message result;
        try {
            result = this.callHandlingMethod();
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
            result = this.protocol.defaultErrorMessage();
        }
        this.context.onFinishedTask(result);
    }
}