package com.litehost.core.dispatch;

import com.litehost.core.MessageHandler;
import com.litehost.core.connection.Context;
import com.litehost.core.connection.transfer.Message;
import com.litehost.core.connection.transfer.MessageBuffer;
import com.litehost.core.connection.transfer.MessageTransferProtocol;
import org.slf4j.Logger;

import java.net.InetSocketAddress;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public abstract sealed class Session extends Context permits ClientSession {
    private final LocalDateTime startTime;
    private final Logger logger;
    private final long timeout;
    private boolean active; //todo implement timeout feature

    protected Session(
            MessageBuffer input, MessageHandler messageHandler,
            InetSocketAddress remoteAddress, ExecutorService taskExecutor,
            MessageTransferProtocol<? super Message> protocol,
            LocalDateTime startTime, Logger logger, long timeout
    ) {
        super(input, messageHandler, remoteAddress, taskExecutor, protocol);
        this.startTime = startTime;
        this.logger = logger;
        this.timeout = timeout;
    }

    public static Session start(
            MessageBuffer input, MessageHandler messageHandler,
            InetSocketAddress remoteAddress, ExecutorService taskExecutor,
            MessageTransferProtocol<? super Message> protocol,
            long timeout, TimeUnit unit, SessionFactory.SessionType sessionType
    ) {
        return sessionType.start(
                input, messageHandler, remoteAddress,
                taskExecutor, protocol, timeout, unit
        );
    }


    public LocalDateTime getStartTime() {
        return this.startTime;
    }

    public Logger getLogger() {
        return this.logger;
    }

    public long getTimeout() {
        return this.timeout;
    }

    public boolean isActive() {
        return this.active;
    }

}

