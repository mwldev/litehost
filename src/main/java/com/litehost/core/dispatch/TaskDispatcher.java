package com.litehost.core.dispatch;

import com.litehost.core.connection.Connectable;
import com.litehost.core.connection.ConnectionController;
import com.litehost.core.connection.Descriptor;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Iterator;

public class TaskDispatcher extends Thread { // todo add some neat configuration system
    private final SessionFactory sessionFactory;
    private volatile boolean running;
    private final Logger logger;
    private final ConnectionController connectionController;
    private final ActionMapper actionMapper;

    public TaskDispatcher(
            SessionFactory sessionFactory,
            Logger logger, ConnectionController controller
    ) {
        this.sessionFactory = sessionFactory;
        this.logger = logger;
        this.connectionController = controller;
        this.actionMapper = ActionMapper.standardActionMapper(this, connectionController);
    }

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    @Override
    public void start() {
        this.setName("Server");
        this.running = true;
        this.setDaemon(false);
        this.setUncaughtExceptionHandler((ExceptionHandlers.stopHandler()));
        long zeroTime = System.nanoTime();
        System.out.println(" Server staring up..."); // todo nice printing required
        try {
            this.sessionFactory.init();
            System.out.println(" === Endpoints mapped ===\n");
        } catch (Exception e) { // looks bad, but the idea is that EVERY exception thrown here terminates the program
            System.err.println(" " + e.getMessage());
            System.err.println(" === Startup aborted ===\n");
            System.exit(420);
        }
        super.start();
        System.out.println(" Finished starting up, elapsed time: " + (System.nanoTime() - zeroTime) / 1_000_000 + "ms");
        this.logger.info(" Server successfully started");
    }

    public void requestStop() {
        this.running = false;
    }

    @Override
    public void run() {
        while (this.running) {
            try {
                Iterator<Descriptor<? extends Connectable, ?>> iterator = this.connectionController.awaitReady().iterator();
                while (iterator.hasNext()) {
                    Descriptor<? extends Connectable, ?> descriptor = iterator.next();
                    iterator.remove();
                    this.actionMapper.actionFor(descriptor.readyOps()).perform(descriptor);
                }
            } catch (IOException e) {
                this.logger.error(e.getMessage());
            }
        }
    }
}
