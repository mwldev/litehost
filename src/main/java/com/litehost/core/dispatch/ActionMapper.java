package com.litehost.core.dispatch;

import com.litehost.core.connection.ConnectionController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ActionMapper {
    private final DispatcherAction[] actions;
    private final Logger logger;

    private ActionMapper() {
        this.logger = LoggerFactory.getLogger(ActionMapper.class);
        this.actions = new DispatcherAction[6];
    }

    public static ActionMapper standardActionMapper(TaskDispatcher dispatcher, ConnectionController connectionController) {
        return ActionMapper.newMapper()
                .map(OperationType.CONNECT, new DispatcherActionOnConnectReady(dispatcher, connectionController))
                .map(OperationType.ACCEPT, new DispatcherActionOnAcceptReady(dispatcher, connectionController))
                .map(OperationType.READ, new DispatcherActionOnReadReady(dispatcher, connectionController))
                .map(OperationType.NOP, new NopOnNoneReady(dispatcher, connectionController)); //not sure about that nop
    }

    public static ActionMapper newMapper() {
        return new ActionMapper();
    }

    public ActionMapper map(OperationType operationType, DispatcherAction action) {
        this.actions[operationType.code()] = action;
        return this;
    }

    public DispatcherAction actionFor(OperationType operationType) {
        return this.actions[operationType.code()];
    }

}
