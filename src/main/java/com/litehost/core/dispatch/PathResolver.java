package com.litehost.core.dispatch;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PathResolver {
    private final Pattern numberPattern;
    private final Pattern stringPattern;
    private final Pattern sectionPattern;
    private final Pattern methodPattern;
    private final Map<String, Path> genericSignatures;

    private PathResolver() {
        this.numberPattern = Pattern.compile("-?\\d+.");
        this.stringPattern = Pattern.compile("[a-zA-Z]");
        this.sectionPattern = Pattern.compile("/\\{?\\w+(\\.\\w+)*");
        this.methodPattern = Pattern.compile("[A-Z]+");
        this.genericSignatures = new HashMap<>();
    }

    public static PathResolver create(Set<Path> signatures) {
        PathResolver resolver = new PathResolver();
        signatures.forEach(path -> resolver.genericSignatures.put(path.getPath(), path));
        signatures.forEach(signature -> {
            Matcher matcher = resolver.sectionPattern.matcher(signature.getPath());
            int index = 0;
            while (matcher.find()) {
                String s = matcher.group();
                ++index;
                if (!s.contains("{")) {
                    Section.createSection(matcher.group(), index);
                }
            }
        });
        return resolver;
    }

    public Path resolveGeneric(String signature, Map<String, Object> pathVariables) { // should take some neat custom type instead of plain Object
        Matcher methodMatcher = this.methodPattern.matcher(signature);
        methodMatcher.find();
        Matcher matcher = this.sectionPattern.matcher(signature);
        StringBuilder builder = new StringBuilder(methodMatcher.group());
        int i = 0;
        List<Object> variablesValues = new ArrayList<>();
        while (matcher.find()) {
            Section section = Section.valueOf(matcher.group());
            if (!section.isConcreteOn(++i)) {
                builder.append("/{");
                Type type = Type.valueOf(this.detectType(section.value));
                variablesValues.add(type.parse(section.value));
                builder.append(type.name())
                        .append("}");
            } else {
                builder.append(section.value);
            }
        }
        String done = builder.toString();
        boolean lastOptional = false;
        if (!this.genericSignatures.containsKey(done)) {
            done = Section.isLastOptional(done, this);
            lastOptional = true;
        }
        if (done != null) {
            int limit = variablesValues.size();
            if (lastOptional) {
                limit--;
            }
            Path path = this.genericSignatures.get(done);
            for (int j = 0; j < limit; j++) {
                pathVariables.put(path.nameAt(j), variablesValues.get(j));
            }
        }
        return this.genericSignatures.get(done);
    }

    private String detectType(String section) {
        Matcher intMatcher = this.numberPattern.matcher(section);
        String type = null;
        if (intMatcher.find()) {
            String found = intMatcher.group();
            if (found.length() == section.length() - 1) {
                type = "INTEGER";
            } else {
                Matcher stringMatcher = this.stringPattern.matcher(section);
                if (stringMatcher.find()) {
                    type = "STRING";
                } else if (section.contains(".")) {
                    type = "FLOAT";
                }
            }
        } else {
            Matcher stringMatcher = this.stringPattern.matcher(section);
            if (stringMatcher.find()) {
                type = "STRING";
            }
        }
        return type;
    }

    private static class Section {
        private Set<Integer> concretePositions = null;
        private final String value;
        private static final Map<String, Section> uniqueValues = new HashMap<>();

        private Section(String value, int index) {
            this.concretePositions = new HashSet<>();
            this.concretePositions.add(index);
            this.value = value;
        }

        private Section(String value) {
            this.value = value;
        }

        public static String isLastOptional(String path, PathResolver resolver) {
            int end = path.lastIndexOf('/');
            String temp = path.substring(0, end);
            if (resolver.genericSignatures.containsKey(temp)) {
                return temp;
            } else return null;
        }

        public static Section valueOf(String value) {
            Section result = Section.uniqueValues.get(value);
            if (result == null) {
                return new Section(value);
            }
            return result;
        }

        public static void createSection(String value, int index) {
            Section section = Section.uniqueValues.get(value);
            if (section != null) {
                section.concretePositions.add(index);
            } else {
                Section result = new Section(value, index);
                Section.uniqueValues.put(value, result);
            }
        }

        public boolean isConcreteOn(int position) {
            if (this.concretePositions != null) {
                return this.concretePositions.contains(position);
            } else return false;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Section section = (Section) o;
            return concretePositions.equals(section.concretePositions) &&
                    Objects.equals(value, section.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(concretePositions, value);
        }
    }

    public enum Type {
        INTEGER {
            @Override
            public Object parse(String data) {
                return Integer.parseInt(data.substring(1));
            }
        }, FLOAT {
            @Override
            public Object parse(String data) {
                return Float.parseFloat(data.substring(1));
            }
        }, STRING {
            @Override
            public Object parse(String data) {
                return data.substring(1);
            }
        };

        public abstract Object parse(String data);
    }


}

