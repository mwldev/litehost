package com.litehost.core.dispatch;

import com.litehost.core.NotMessageException;
import com.litehost.core.connection.transfer.Message;
import org.atteo.classindex.ClassIndex;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Endpoints {
    /*  some good code from a wrong place, implement it here
     private void initAcceptors() {
        this.acceptors.forEach(((integer, serverSocketChannel) -> {
            try {
                serverSocketChannel.bind(new InetSocketAddress(
                        this.connectionController.getLocalBindAddress(), integer
                ));
                this.connectionController.register(
                        ConnectionAcceptor.newAcceptor(serverSocketChannel), EnumSet.of(OperationType.ACCEPT)
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }
    */
    //todo map protocols
    public static Map<Path, Endpoint> endpointMap() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Iterable<Class<?>> classes = ClassIndex.getAnnotated(com.litehost.core.annotate.Endpoint.class);
        Map<Path, Endpoint> mapping = new HashMap<>();
        Pattern pathVariablePattern = Pattern.compile("\\{\\w+(, ?TYPE:[A-Z]+)?}");
        StringBuilder signature = new StringBuilder();
        Class<Message> messageCheck = Message.class;
        Map<String, String> variablesCheck = new HashMap<>();
        for (Class<?> clazz : classes) {
            com.litehost.core.annotate.Endpoint marker = clazz.getAnnotation(com.litehost.core.annotate.Endpoint.class);
            Constructor<?> constructor = clazz.getConstructor();
            Object instance = constructor.newInstance();
            String parentPath = marker.path();
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                marker = method.getAnnotation(com.litehost.core.annotate.Endpoint.class);
                if (marker != null) {
                    Parameter[] parameters = method.getParameters();
                    if (parameters.length > 0) {
                        if (parameters.length > 2) {
                            throw new IllegalArgumentException("Only one Message and Map<String, PathVariable> can parameter, or no parameters can be passed");
                        }
                        for (Parameter parameter : parameters) {
                            if (!(parameter.getType().isInstance(variablesCheck) || parameter.getType().isAssignableFrom(messageCheck))) {
                                throw new IllegalArgumentException("Parameters must be a Message, or Message and Map!");
                            }
                        }
                    }
                    if (!method.getReturnType().isAssignableFrom(messageCheck)) {
                        throw new NotMessageException("Handling method return type must be a Message");
                    }
                    String prefix = marker.prefix();
                    signature.append(prefix);
                    signature.append(parentPath);
                    signature.append(marker.path());
                    Endpoints.mapToEndpoint(pathVariablePattern, signature.toString(), mapping, method, instance);
                }
            }
        }
        return mapping;
    }

    private static void mapToEndpoint(Pattern pattern, String signature, Map<Path, Endpoint> mapping, Method method, Object instance) {
        Path path = null;
        if (signature.contains("{")) {
            List<String> names = new ArrayList<>();
            Matcher matcher = pattern.matcher(signature);
            while (matcher.find()) {
                String oldVariable = matcher.group();
                StringBuilder newVariable = new StringBuilder();
                if (!oldVariable.contains("TYPE")) {
                    newVariable.append("{STRING}");
                    names.add(oldVariable.substring(1, oldVariable.length() - 1));
                } else {
                    int startIndex = oldVariable.indexOf(":") + 1;
                    int separatorIndex = oldVariable.indexOf(',');
                    newVariable.append('{')
                            .append(oldVariable, startIndex, oldVariable.length() - 1)
                            .append('}');
                    names.add(oldVariable.substring(1, separatorIndex));
                }
                signature = signature.replace(oldVariable, newVariable);
                path = new Path(signature);
                path.setNames(names);
            }
        } else {
            path = new Path(signature);
        }
        com.litehost.core.dispatch.Endpoint endpoint = new com.litehost.core.dispatch.Endpoint(path, method, instance);
        mapping.put(path, endpoint);
    }
}
