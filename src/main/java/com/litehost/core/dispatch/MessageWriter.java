package com.litehost.core.dispatch;

import com.litehost.core.MessageHandler;
import com.litehost.core.connection.Connection;
import com.litehost.core.connection.Context;
import com.litehost.core.connection.transfer.Message;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MessageWriter extends MessageHandler {

    public MessageWriter(Logger logger, Selector selector) {
        super(logger, selector);
    }

    @Override
    public synchronized void start() {
        this.setDaemon(true);
        this.setUncaughtExceptionHandler(ExceptionHandlers.stopHandler());
        this.running = true;
        super.start();
        System.out.println("\n === Listener started ===\n");
    }

    private void writeToSelected() {
        Set<SelectionKey> selectedKeys = this.selector.selectedKeys();
        Iterator<SelectionKey> iterator = selectedKeys.iterator();
        while (iterator.hasNext()) {
            SelectionKey selectedKey = iterator.next();
            iterator.remove();
            int remaining = this.write(selectedKey);
            if (remaining == 0) {
                selectedKey.cancel();
            }
        }
    }

    private int write(SelectionKey selectionKey) {
        MessageHandler.Delegation delegation = (MessageHandler.Delegation) selectionKey.attachment();
        Connection connection = delegation.connection();
        ByteBuffer data = delegation.data();
        return connection.write(data);
    }

    @Override
    public void run() {
        while (this.running) {
            try {
                this.antiSpinLock.lock();
                while (!this.hasDelegations()) {
                    this.hasDelegations.await();
                }
                this.selector.select();
                this.writeToSelected();
            } catch (IOException | InterruptedException e) {
                this.logger.error(e.getMessage());
            } finally {
                this.antiSpinLock.unlock();
            }
        }
        this.onStopRequest();
    }

    @Override
    protected void onStopRequest() {

    }
}

