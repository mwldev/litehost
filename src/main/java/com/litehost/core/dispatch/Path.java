package com.litehost.core.dispatch;

import java.util.List;

public class Path {
    private final String path;
    private List<String> names;

    public Path(String path) {
        this.path = path;
    }


    public Path setNames(List<String> names) {
        this.names = names;
        return this;
    }

    public String nameAt(int i) {
        if (this.names != null) {
            return this.names.get(i);
        } else return null;
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }

    public String getPath() {
        return this.path;
    }
}
