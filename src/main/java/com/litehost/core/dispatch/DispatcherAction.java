package com.litehost.core.dispatch;

import com.litehost.core.connection.*;
import com.litehost.core.connection.Connectable;
import com.litehost.core.connection.transfer.MessageBuffer;

import java.io.IOException;
import java.net.InetSocketAddress;

public abstract class DispatcherAction {
    private final TaskDispatcher taskDispatcher;
    private final ConnectionController connectionController;

    protected DispatcherAction(TaskDispatcher taskDispatcher, ConnectionController connectionController) {
        this.taskDispatcher = taskDispatcher;
        this.connectionController = connectionController;
    }

    public final TaskDispatcher getDispatcher() {
        return this.taskDispatcher;
    }

    public final ConnectionController getConnectionController() {
        return this.connectionController;
    }

    public abstract void perform(Descriptor<? extends Connectable, ?> descriptor) throws IOException;
}

class DispatcherActionOnAcceptReady extends DispatcherAction {
    protected DispatcherActionOnAcceptReady(TaskDispatcher taskDispatcher, ConnectionController connectionController) {
        super(taskDispatcher, connectionController);
    }

    @Override
    public void perform(Descriptor<? extends Connectable, ?> descriptor) throws IOException {
        Connection connection = ((ConnectionAcceptorDescriptor) descriptor)
                .connectable()
                .accept();
        String protocol = "" + ((InetSocketAddress) connection.getChannel().getLocalAddress()).getPort();
        Context context = this.getDispatcher()
                .getSessionFactory()
                .startSession(
                        MessageBuffer.create(2 * 1024),
                        (InetSocketAddress) connection.getChannel().getRemoteAddress(),
                        protocol, SessionFactory.SessionType.LOCAL
                );
        this.getConnectionController().register(connection, context, OperationType.READ);
    }
}

class DispatcherActionOnConnectReady extends DispatcherAction {

    protected DispatcherActionOnConnectReady(TaskDispatcher taskDispatcher, ConnectionController connectionController) {
        super(taskDispatcher, connectionController);
    }

    @Override
    public void perform(Descriptor<? extends Connectable, ?> descriptor) throws IOException {
        descriptor.changeInterest(OperationType.READ);
    }

}

class DispatcherActionOnReadReady extends DispatcherAction {
    protected DispatcherActionOnReadReady(TaskDispatcher taskDispatcher, ConnectionController connectionController) {
        super(taskDispatcher, connectionController);
    }

    @Override
    public void perform(Descriptor<? extends Connectable, ?> descriptor) throws IOException {
        ConnectionDescriptor connectionDescriptor = ((ConnectionDescriptor) descriptor);
        connectionDescriptor.context().onDataReceived(connectionDescriptor.connectable());
    }
}

class NopOnNoneReady extends DispatcherAction {

    protected NopOnNoneReady(TaskDispatcher taskDispatcher, ConnectionController connectionController) {
        super(taskDispatcher, connectionController);
    }

    @Override
    public void perform(Descriptor<? extends Connectable, ?> descriptor) throws IOException {
    }
}