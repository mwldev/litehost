package com.litehost.core.dispatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionHandlers {
    private ExceptionHandlers() {
    }

    public static Thread.UncaughtExceptionHandler logHandler() {
        return new LogHandler();
    }

    static Thread.UncaughtExceptionHandler stopHandler() {
        return (t, e) -> {
            logHandler().uncaughtException(t, e);
            System.exit(420);
        };
    }

    private static class LogHandler implements Thread.UncaughtExceptionHandler {
        private String additionalMessage = null;

        LogHandler() {
        }


        public void additionalMessage(String msg) {
            this.additionalMessage = msg;
        }

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            StringBuilder cause = new StringBuilder(e.getClass().getSimpleName());
            cause.append(' ');
            cause.append(e.getStackTrace()[0].toString());
            Logger logger = LoggerFactory.getLogger(TaskDispatcher.class);
            if (this.additionalMessage != null)
                logger.debug("Exception " + cause + "at " + t.getName() + ", information: " + this.additionalMessage);
            else logger.debug("Exception " + cause + "at " + t.getName());
        }
    }
}
