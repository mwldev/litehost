package com.litehost.core.dispatch;

import com.litehost.core.MessageHandler;
import com.litehost.core.connection.transfer.Message;
import com.litehost.core.connection.transfer.MessageBuffer;
import com.litehost.core.connection.transfer.MessageTransferProtocol;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class SessionFactory {
    private final MessageWriter messageHandler;
    private final ExecutorService taskExecutor;
    private final Map<String, MessageTransferProtocol<? super Message>> networkProtocols; //changed key type to string in order to enable neat protocol addition, that is by custom name
    private PathResolver pathResolver;
    private int timeout;
    private TimeUnit timeUnit;

    public enum SessionType {
        LOCAL {
            @Override
            public Session start(
                    MessageBuffer input, MessageHandler messageHandler,
                    InetSocketAddress remoteAddress, ExecutorService taskExecutor,
                    MessageTransferProtocol<? super Message> protocol,
                    long timeout, TimeUnit unit) {
                return new ClientSession(
                        input, messageHandler,
                        remoteAddress, taskExecutor,
                        protocol, LocalDateTime.now(),
                        LoggerFactory.getLogger(ClientSession.class),
                        unit.toNanos(timeout)
                );
            }
        }, REMOTE {
            @Override
            public Session start(
                    MessageBuffer input, MessageHandler messageHandler,
                    InetSocketAddress remoteAddress, ExecutorService taskExecutor,
                    MessageTransferProtocol<? super Message> protocol,
                    long timeout, TimeUnit unit) {
                return null;
            }
        };

        public abstract Session start(
                MessageBuffer input, MessageHandler messageHandler,
                InetSocketAddress remoteAddress, ExecutorService taskExecutor,
                MessageTransferProtocol<? super Message> protocol,
                long timeout, TimeUnit unit);
    }

    public SessionFactory(
            MessageWriter messageWriter, ExecutorService taskExecutor,
            Map<String, MessageTransferProtocol<? super Message>> networkProtocols, int timeout, TimeUnit timeUnit
    ) {
        this.messageHandler = messageWriter;
        this.taskExecutor = taskExecutor;
        this.networkProtocols = networkProtocols;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
    }

    public SessionFactory(MessageWriter messageWriter, ExecutorService taskExecutor, Map<String, MessageTransferProtocol<? super Message>> networkProtocols) {
        this(messageWriter, taskExecutor, networkProtocols, 60, TimeUnit.SECONDS);
    }

    public static SessionFactory newFactory(
            MessageWriter messageWriter, ExecutorService taskExecutor,
            Map<String, MessageTransferProtocol<? super Message>> networkProtocols
    ) {
        return new SessionFactory(messageWriter, taskExecutor, networkProtocols);
    }

    public SessionFactory init() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Map<Path, Endpoint> mapping = Endpoints.endpointMap();
        this.pathResolver = PathResolver.create(mapping.keySet());
        this.messageHandler.start();
        return this;
    }

    public Session startSession(
            MessageBuffer input, InetSocketAddress remoteAddress,
            String protocol, SessionType sessionType
    ) {
        return sessionType.start(
                input, this.messageHandler,
                remoteAddress, this.taskExecutor,
                this.networkProtocols.get(protocol), this.timeout, this.timeUnit
        );
    }
}
