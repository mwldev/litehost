package com.litehost.core;

public class IllegalBufferCapacityException extends BufferCapacityException {
    public IllegalBufferCapacityException(String message) {
        super(message);
    }
}
